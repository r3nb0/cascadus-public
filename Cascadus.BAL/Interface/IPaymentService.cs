﻿using Cascadus.Model.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Cascadus.BAL.Interface
{
    public interface IPaymentService
    {
        Task<Object> CreatePayment(RacunViewModel invoice, OrderViewModel order);
    }
}
