﻿using Cascadus.BAL.Interface;
using Cascadus.Model.Configuration;
using Cascadus.Model.Models.DBModels;
using Cascadus.Model.Models.ViewModels;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Cascadus.BAL.Implementation
{
    public class PaymentService : IPaymentService
    {
        private readonly IConfiguration _configuration;
        JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true };

        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IRacunService _racunService;
        private readonly IKupacService _kupacService;
        private readonly IProizvodService _proizvodService;
        private readonly IStavkaService _stavkaService;

        public PaymentService(
            IRacunService racunService,
            IKupacService kupacService,
            IProizvodService proizvodService,
            IStavkaService stavkaService, 
            IHttpClientFactory httpClientFactory)
        {
            _racunService = racunService;
            _kupacService = kupacService;
            _proizvodService = proizvodService;
            _stavkaService = stavkaService;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<object> CreatePayment(RacunViewModel invoice, OrderViewModel order)
        {
            var httpClient = _httpClientFactory.CreateClient("Corvus");
            //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);


            CorvusPaymentViewModel paymentModel =
                   new CorvusPaymentViewModel(
                       order.OrderNumber.ToString(),
                       order.PaymentModel.Amount,
                       order.PaymentModel.GetFormattedCartItems());

            //var response = await _httpClient
            //    .PostAsync(Configuration.CORVUSPAY_CHECKOUT_URL, );


            var response = await httpClient.PostAsync(Configuration.CORVUSPAY_CHECKOUT_URL, new StringContent(paymentModel.ToString()));
            //if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            //    throw new Exception("401");

            var result = await response.Content.ReadAsStringAsync();
            //return JsonSerializer.Deserialize<List<AuthorizedCompanyResult>>(result, jsonSerializerOptions);



            if (response.IsSuccessStatusCode)
            {
                return "success";
            }
            else
            {
                return "fail";
            }
        }
    }
}
