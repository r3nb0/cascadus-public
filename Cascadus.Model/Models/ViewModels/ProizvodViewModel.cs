﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascadus.Model.Models.ViewModels
{
    public class ProizvodViewModel
    {
        public int Id { get; set; }
        public int KategorijaId { get; set; }
        public String Naziv { get; set; }
        public String Kategorija { get; set; }
        public String Putanja { get; set; }
        public String Karakteristike { get; set; }
        public String Opis { get; set; }
        public String Thumbnail { get; set; }
        public Decimal Cijena { get; set; }
        public int Kolicina { get; set; }
        public bool Izbrisano { get; set; }
    }
}
