﻿using System;

namespace Cascadus.Model.Models.ViewModels
{
    public class PopustKodViewModel
    {
        public int Id { get; set; }
        public string PopustKod { get; set; }
        public int PopustUpostocima { get; set; }
        public bool Izbrisano { get; set; }
        public DateTime VrijediDo { get; set; }
    }
}