﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Cascadus.Model.Models.ViewModels
{
    public class CorvusPaymentViewModel
    {
        public CorvusPaymentViewModel(String orderNumber, float amount, String cart)
        {
            Version = "1.3";
            StoreId = Configuration.Configuration.CORVUSPAY_STORE_ID;
            OrderNumber = orderNumber;
            Language = Configuration.Configuration.CORVUSPAY_LANGUAGE;
            Currency = Configuration.Configuration.CORVUSPAY_CURRENCY;
            Amount = amount;
            Cart = cart;
            RequireComplete = "true";
            Signature = GetSignatureHash();
        }
        public String Version { get; set; }
        public int StoreId { get; set; }
        public String OrderNumber { get; set; }
        public String Language { get; set; }
        public String Currency { get; set; }
        public float Amount { get; set; }
        public String Cart { get; set; }
        public String Signature { get; set; }
        public String RequireComplete { get; set; }

        private String GetSignatureHash()
        {
            // change according to your needs, an UTF8Encoding
            // could be more suitable in certain situations
            ASCIIEncoding encoding = new ASCIIEncoding();

            var keyPairs = new List<KeyValuePair<String, object>>();
            keyPairs.Add(new KeyValuePair<String, object>("amount", Amount));
            keyPairs.Add(new KeyValuePair<String, object>("cart", Cart));
            keyPairs.Add(new KeyValuePair<String, object>("currency", Configuration.Configuration.CORVUSPAY_CURRENCY));
            keyPairs.Add(new KeyValuePair<String, object>("language", Configuration.Configuration.CORVUSPAY_LANGUAGE));
            keyPairs.Add(new KeyValuePair<String, object>("order_number", OrderNumber));
            keyPairs.Add(new KeyValuePair<String, object>("require_complete", Configuration.Configuration.CORVUSPAY_REQUIRED_COMPLETE));
            keyPairs.Add(new KeyValuePair<String, object>("store_id", Configuration.Configuration.CORVUSPAY_STORE_ID));
            keyPairs.Add(new KeyValuePair<String, object>("version", Configuration.Configuration.CORVUSPAY_VERSION));

            Byte[] textBytes = encoding.GetBytes(Configuration.Configuration.CORVUSPAY_SECURITY_KEY);

            StringBuilder sb = new StringBuilder();
            keyPairs.ForEach(x => {
                sb.Append(String.Format("{0}{1}", x.Key, x.Value));
            });
            Byte[] keyBytes = encoding.GetBytes(sb.ToString());

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(keyBytes))
                hashBytes = hash.ComputeHash(textBytes);

            return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
        }

        public override string ToString()
        {
            return String.Format("store id={0}&order number={1}&language={2}&currency={3}&amount={4}&cart={5}&signature={6}&require_complete={7}",
                StoreId, OrderNumber, Language, Currency, Amount, Cart, Signature, RequireComplete);
        }
    }
}
