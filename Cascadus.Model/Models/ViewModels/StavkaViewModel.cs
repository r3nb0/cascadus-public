﻿using Cascadus.Model.Models.DBModels;

namespace Cascadus.Model.Models.ViewModels
{
    public class StavkaViewModel
    {
        public int Id { get; set; }
        public int RacunId { get; set; }
        public int ProizvodId { get; set; }
        public int Kolicina { get; set; }
        public decimal CijenaPoKomadu { get; set; }
        public decimal UkupnaCijena { get; set; }
        public bool Izbrisano { get; set; }
        public int PopustKodId { get; set; }
        public PopustKodViewModel PopustKod { get; set; }
        public ProizvodViewModel Proizvod { get; set; }
    }
}