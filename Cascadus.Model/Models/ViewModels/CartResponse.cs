﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascadus.Model.Models.ViewModels
{
    public enum CartResponseStatusCodes
    {
        SUCCESS,
        FAIL
    }
    public class CartResponse
    {
        public CartResponse(string buyerId, int invoiceId, CartResponseStatusCodes statusCode)
        {
            BuyerId = buyerId;
            InvoiceId = invoiceId;
            StatusCode = statusCode;
        }

        public String BuyerId { get; set; }
        public int InvoiceId { get; set; }
        public CartResponseStatusCodes StatusCode { get; set; }
    }
}
