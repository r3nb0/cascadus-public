﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cascadus.Model.Models.ViewModels
{
    public class RacunViewModel
    {

        public int Id { get; set; }
        public int KupacId { get; set; }
        public String Guid { get; set; }
        public DateTime DatumIzdavanja { get; set; }
        public DateTime? DatumSlanja { get; set; }
        public int BrojRacuna { get; set; }
        public String KomentarNarudzbe { get; set; }
        public String PracenjePosiljke { get; set; }
        public KupacViewModel Kupac { get; set; }
        public bool Izbrisano { get; set; }
        public ICollection<StavkaViewModel> Stavke { get; set; }
        public String NacinPlacanja { get; set; }
    }
}
