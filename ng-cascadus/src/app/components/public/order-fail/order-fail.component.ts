import { BuyersService } from './../../../sevices/buyers.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Buyer } from 'src/app/models/buyer';

@Component({
  selector: 'app-order-fail',
  templateUrl: './order-fail.component.html',
  styleUrls: ['./order-fail.component.scss']
})
export class OrderFailComponent implements OnInit {

  buyer: Buyer;
  id: String;

  constructor(private route: ActivatedRoute,) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      var name = params.get('buyer');
      name = name.charAt(0).toUpperCase() + name.slice(1);
      document.getElementById('buyerName').innerText = name as string;
    });
  }

}
