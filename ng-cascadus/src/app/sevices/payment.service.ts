import { Order } from 'src/app/models/order';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class PaymentService {
    private URL:String;

    constructor(private http: HttpClient) {
        this.URL = environment.domain + "public/cart/";
    }
    
    pay(order: Order) : Observable<String> {
        return this.http.post<String>(this.URL + "placeOrder", order, {  headers: new HttpHeaders({
            "Content-Type": "application/json" 
        })});
    }
}