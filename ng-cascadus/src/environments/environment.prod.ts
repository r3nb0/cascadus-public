export const environment = {
  production: true,
  domain: "https://api.cascadus.hr/api/",
  tokenKey: "jwt",
  roleKey: "role"
};
