
using Cascadus.BAL.Implementation;
using Cascadus.BAL.Interface;
using Cascadus.DAL.Implementation;
using Cascadus.DAL.Interface;
using Cascadus.Model.Models.ViewModels;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Net.Http;
using System.Text;

namespace Cascadus.WebAPI
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            #region Bruno
            //services.AddCors(options =>
            //{
            //    options.AddPolicy(MyAllowSpecificOrigins,
            //    builder =>
            //    {
            //        builder.SetIsOriginAllowed(isOriginAllowed: _ => true).AllowAnyHeader().AllowAnyMethod().AllowCredentials();
            //    });
            //});

            //// Authentication
            //services.AddAuthentication(opt =>
            //{
            //    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(options =>
            //{
            //    var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Token:PrivateKey"]));
            //    options.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        IssuerSigningKey = signingKey,
            //        ValidIssuer = Configuration["Token:Issuer"],
            //        ValidAudience = Configuration["Token:Audience"],
            //        RequireExpirationTime = true,
            //        ValidateIssuer = false,
            //        ValidateAudience = false,
            //        ValidateLifetime = true,
            //        ValidateIssuerSigningKey = true,
            //    };
            //});

            //services.AddControllers();
            //services.AddMemoryCache();
            //services.AddHttpClient();
            #endregion

            //CORS
            //services.AddCors(options =>
            //{
            //    options.AddPolicy(MyAllowSpecificOrigins,
            //    builder =>
            //    {
            //        builder
            //        .AllowAnyOrigin()
            //        .AllowAnyHeader()
            //        .AllowAnyMethod()
            //        .AllowCredentials()
            //        .WithOrigins("*");
            //        //    builder
            //        //    .AllowAnyHeader()
            //        //    .AllowAnyMethod()
            //        //    .AllowCredentials()
            //        //    .WithOrigins(
            //        //        "https://localhost:44326",
            //        //        "http://cascadus.hr",
            //        //        "https://cascadus.hr");
            //        //});
            //    });
            //});
            //BRUNO CORS
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.SetIsOriginAllowed(isOriginAllowed: _ => true).AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                });
            });

            var jwtSigningKey = Configuration
                    .GetSection("JWTSigningKey")
                    .Get<JwtSigningKey>();
                services.AddSingleton(jwtSigningKey);

                services.AddAuthentication(opt =>
                {
                    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,

                            ValidIssuer = "https://api.cascadus.hr",
                            ValidAudience = "https://cascadus.hr",
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSigningKey.Key))
                        };
                    });

                var emailConfiguration = Configuration
                    .GetSection("MailSettings")
                    .Get<EmailConfiguration>();
                services.AddSingleton(emailConfiguration);

                //services.http
                services.AddHttpClient("Corvus", c =>
                {
                    c.BaseAddress = new Uri(Configuration["Corvus:BaseUrl"]);
                    c.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                    c.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                });


                services.Configure<FormOptions>(o =>
                {
                    o.ValueLengthLimit = int.MaxValue;
                    o.MultipartBodyLengthLimit = int.MaxValue;
                    o.MemoryBufferThreshold = int.MaxValue;
                });

                services.AddSingleton<IKupacService, KupacService>();
                services.AddSingleton<IKupacRepo, KupacRepository>();

                services.AddTransient<IProizvodService, ProizvodService>();
                services.AddSingleton<IProizvodRepo, ProizvodRepository>();

                services.AddTransient<IKategorijeProizvodaService, KategorijaProizvodaService>();
                services.AddSingleton<IKategorijaProizvodaRepo, KategorijaProizvodaRepository>();

                services.AddTransient<IStavkaService, StavkaService>();
                services.AddSingleton<IStavkaRepo, StavkaRepository>();

                services.AddTransient<IRacunService, RacunService>();
                services.AddSingleton<IRacunRepo, RacunRepository>();

                services.AddTransient<IKorisnickiRacunService, KorisnickiRacunService>();
                services.AddSingleton<IKorisnickiRacunRepo, KorisnickiRacunRepository>();

                services.AddTransient<IPopustKodService, PopustKodService>();
                services.AddSingleton<IPopustKodRepo, PopustKodRepository>();

                services.AddSingleton<IPaymentService, PaymentService>();

                services.AddScoped<IMailService, MailService>();
                services.AddTransient<EmailRepository>();

                services.AddControllers();
            }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
            {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            //BOREN
            //app.UseForwardedHeaders(new ForwardedHeadersOptions
            //{
            //    ForwardedHeaders = ForwardedHeaders.XForwardedFor |
            //    ForwardedHeaders.XForwardedProto
            //});

            //app.UseCors(MyAllowSpecificOrigins);

            //app.UseHttpsRedirection();
            //app.UseRouting();

            ////JWT TOKENS
            //app.UseAuthentication();
            //app.UseAuthorization();


            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});


            //// NEW - BRUNO
            app.UseHttpsRedirection();
            app.UseRouting();


            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
               ForwardedHeaders.XForwardedProto
            });


            app.UseAuthentication();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });



        }



    }
    }
